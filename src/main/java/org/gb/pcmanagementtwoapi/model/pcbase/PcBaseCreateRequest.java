package org.gb.pcmanagementtwoapi.model.pcbase;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import org.gb.pcmanagementtwoapi.enums.RunStatus;

@Getter
@Setter
public class PcBaseCreateRequest {
    @Enumerated(value = EnumType.STRING)
    private RunStatus runStatus;
}
