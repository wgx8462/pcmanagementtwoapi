package org.gb.pcmanagementtwoapi.service;

import lombok.RequiredArgsConstructor;
import org.gb.pcmanagementtwoapi.entity.Pcbase;
import org.gb.pcmanagementtwoapi.entity.UpdatePc;
import org.gb.pcmanagementtwoapi.model.updatepc.UpdatePcChangeRequest;
import org.gb.pcmanagementtwoapi.model.updatepc.UpdatePcCreateRequest;
import org.gb.pcmanagementtwoapi.model.updatepc.UpdatePcItem;
import org.gb.pcmanagementtwoapi.model.updatepc.UpdatePcResponse;
import org.gb.pcmanagementtwoapi.repository.UpdatePcRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UpdatePcService {
    private final UpdatePcRepository updatePcRepository;

    public void setUpdatePc(Pcbase pcbase, UpdatePcCreateRequest request) {
        UpdatePc addData = new UpdatePc();

        addData.setPcBase(pcbase);
        addData.setUpdateType(request.getUpdateType());
        addData.setDateUpCheck(LocalDate.now());
        updatePcRepository.save(addData);
    }

    public List<UpdatePcItem> getUpdatePcs() {
        List<UpdatePc> originList = updatePcRepository.findAll();
        List<UpdatePcItem> result = new LinkedList<>();

        for (UpdatePc updatePc : originList) {
            UpdatePcItem addItem = new UpdatePcItem();

            addItem.setPcbaseRunStatus(updatePc.getPcBase().getRunStatus().getName());
            addItem.setUpdateType(updatePc.getUpdateType().getName());
            addItem.setDateUpCheck(updatePc.getDateUpCheck());
            result.add(addItem);
        }
        return result;
    }

    public UpdatePcResponse getUpdatePc(long id) {
        UpdatePc originData = updatePcRepository.findById(id).orElseThrow();
        UpdatePcResponse response = new UpdatePcResponse();

        response.setPcbaseRunStatus(originData.getPcBase().getRunStatus().getName());
        response.setId(originData.getId());
        response.setUpdateType(originData.getUpdateType().getName());
        response.setDateUpCheck(originData.getDateUpCheck());
        return response;

    }

    public void putChange(long id, UpdatePcChangeRequest request) {
        UpdatePc originData = updatePcRepository.findById(id).orElseThrow();

        originData.setUpdateType(request.getUpdateType());
        updatePcRepository.save(originData);
    }

    public void delUpdatePc(long id) {
        updatePcRepository.deleteById(id);
    }
}
