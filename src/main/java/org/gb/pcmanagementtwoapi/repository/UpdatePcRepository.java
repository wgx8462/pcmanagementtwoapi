package org.gb.pcmanagementtwoapi.repository;

import org.gb.pcmanagementtwoapi.entity.UpdatePc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UpdatePcRepository extends JpaRepository<UpdatePc, Long> {
}
