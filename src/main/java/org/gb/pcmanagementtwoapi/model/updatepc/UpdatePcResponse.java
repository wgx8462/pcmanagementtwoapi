package org.gb.pcmanagementtwoapi.model.updatepc;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class UpdatePcResponse {
    private String pcbaseRunStatus;
    private Long id;
    private String updateType;
    private LocalDate dateUpCheck;
}
