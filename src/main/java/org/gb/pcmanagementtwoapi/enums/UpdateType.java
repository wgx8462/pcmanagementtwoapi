package org.gb.pcmanagementtwoapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UpdateType {
    UPDATE_WINDOW("윈도우 업데이트"),
    UPDATE_GRAPHICS("그래픽드라이버 업데이트"),
    UPDATE_SOUNDS("사운드드라이버 업데이트"),
    UPDATE_GAME("게임 업데이트");

    private final String name;
}
