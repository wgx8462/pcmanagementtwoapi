package org.gb.pcmanagementtwoapi.model.pcbase;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcbaseResponse {
    private Long id;
    private String runStatus;
    private LocalDate dateSet;
}
