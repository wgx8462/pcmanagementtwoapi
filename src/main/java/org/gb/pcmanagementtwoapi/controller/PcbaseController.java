package org.gb.pcmanagementtwoapi.controller;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.gb.pcmanagementtwoapi.model.pcbase.PcBaseCreateRequest;
import org.gb.pcmanagementtwoapi.model.pcbase.PcbaseItem;
import org.gb.pcmanagementtwoapi.model.pcbase.PcbaseResponse;
import org.gb.pcmanagementtwoapi.model.pcbase.PcbaseRunStatusChangeRequest;
import org.gb.pcmanagementtwoapi.service.PcbaseService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pcbase")
public class PcbaseController {
    private final PcbaseService pcBaseService;

    @PostMapping("/join")
    public String setPcBase(@RequestBody PcBaseCreateRequest request) {
        pcBaseService.setPcBase(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<PcbaseItem> getPcbases() {
        return pcBaseService.getPcbases();
    }

    @GetMapping("/detail/{id}")
    public PcbaseResponse getPcbase(@PathVariable long id) {
        return pcBaseService.getPcbase(id);
    }

    @PutMapping("/run-status/{id}")
    public String putRunStatusChange(@PathVariable long id, @RequestBody PcbaseRunStatusChangeRequest request) {
        pcBaseService.putRunStatusChange(id, request);

        return "OK";
    }
}
