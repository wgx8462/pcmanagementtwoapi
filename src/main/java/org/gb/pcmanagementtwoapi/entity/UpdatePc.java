package org.gb.pcmanagementtwoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.gb.pcmanagementtwoapi.enums.UpdateType;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class UpdatePc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pcBaseId", nullable = false)
    private Pcbase pcBase;

    @Column(nullable = false, length = 25)
    @Enumerated(value = EnumType.STRING)
    private UpdateType updateType;

    @Column(nullable = false)
    private LocalDate dateUpCheck;
}
