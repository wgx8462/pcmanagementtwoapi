package org.gb.pcmanagementtwoapi.service;

import lombok.RequiredArgsConstructor;
import org.gb.pcmanagementtwoapi.entity.Pcbase;
import org.gb.pcmanagementtwoapi.model.pcbase.PcBaseCreateRequest;
import org.gb.pcmanagementtwoapi.model.pcbase.PcbaseItem;
import org.gb.pcmanagementtwoapi.model.pcbase.PcbaseResponse;
import org.gb.pcmanagementtwoapi.model.pcbase.PcbaseRunStatusChangeRequest;
import org.gb.pcmanagementtwoapi.repository.PcbaseRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcbaseService {
    private final PcbaseRepository pcBaseRepository;

    public Pcbase getData(long id) {
        return pcBaseRepository.findById(id).orElseThrow();
    }

    public void setPcBase(PcBaseCreateRequest request) {
        Pcbase addData = new Pcbase();

        addData.setRunStatus(request.getRunStatus());
        addData.setDateSet(LocalDate.now());

        pcBaseRepository.save(addData);
    }

    public List<PcbaseItem> getPcbases() {
        List<Pcbase> originList = pcBaseRepository.findAll();
        List<PcbaseItem> result = new LinkedList<>();

        for (Pcbase pcbase : originList) {
            PcbaseItem addItem = new PcbaseItem();
            addItem.setId(pcbase.getId());
            addItem.setRunStatus(pcbase.getRunStatus().getName());
            result.add(addItem);
        }
        return result;
    }

    public PcbaseResponse getPcbase(long id) {
        Pcbase originData = pcBaseRepository.findById(id).orElseThrow();
        PcbaseResponse response = new PcbaseResponse();

        response.setId(originData.getId());
        response.setRunStatus(originData.getRunStatus().getName());
        response.setDateSet(originData.getDateSet());
        return response;
    }

    public void putRunStatusChange(long id, PcbaseRunStatusChangeRequest request) {
        Pcbase originData = pcBaseRepository.findById(id).orElseThrow();

        originData.setRunStatus(request.getRunStatus());

        pcBaseRepository.save(originData);
    }

}
