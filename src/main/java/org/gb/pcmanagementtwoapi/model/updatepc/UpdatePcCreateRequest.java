package org.gb.pcmanagementtwoapi.model.updatepc;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import org.gb.pcmanagementtwoapi.enums.UpdateType;

@Getter
@Setter
public class UpdatePcCreateRequest {
    @Enumerated(value = EnumType.STRING)
    private UpdateType updateType;
}
