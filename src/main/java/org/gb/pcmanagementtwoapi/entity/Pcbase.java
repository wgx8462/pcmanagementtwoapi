package org.gb.pcmanagementtwoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.gb.pcmanagementtwoapi.enums.RunStatus;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Pcbase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 25)
    @Enumerated(value = EnumType.STRING)
    private RunStatus runStatus;

    @Column(nullable = false)
    private LocalDate dateSet;
}
