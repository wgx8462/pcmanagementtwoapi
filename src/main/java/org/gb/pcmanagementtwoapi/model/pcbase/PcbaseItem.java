package org.gb.pcmanagementtwoapi.model.pcbase;

import lombok.Getter;
import lombok.Setter;
import org.gb.pcmanagementtwoapi.enums.RunStatus;

@Getter
@Setter
public class PcbaseItem {
    private Long id;
    private String runStatus;
}
