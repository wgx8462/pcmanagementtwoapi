package org.gb.pcmanagementtwoapi.repository;

import org.gb.pcmanagementtwoapi.entity.Pcbase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcbaseRepository extends JpaRepository<Pcbase, Long> {
}
