package org.gb.pcmanagementtwoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcManagementTwoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcManagementTwoApiApplication.class, args);
	}

}
