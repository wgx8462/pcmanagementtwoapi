package org.gb.pcmanagementtwoapi.controller;

import lombok.RequiredArgsConstructor;
import org.gb.pcmanagementtwoapi.entity.Pcbase;
import org.gb.pcmanagementtwoapi.model.updatepc.UpdatePcChangeRequest;
import org.gb.pcmanagementtwoapi.model.updatepc.UpdatePcCreateRequest;
import org.gb.pcmanagementtwoapi.model.updatepc.UpdatePcItem;
import org.gb.pcmanagementtwoapi.model.updatepc.UpdatePcResponse;
import org.gb.pcmanagementtwoapi.service.PcbaseService;
import org.gb.pcmanagementtwoapi.service.UpdatePcService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/update-pc")
public class UpdatePcController {
    private final PcbaseService pcbaseService;
    private final UpdatePcService updatePcService;

    @PostMapping("/new/pcbase-id/{pabaseId}")
    public String setUpdatePc(@PathVariable long pabaseId, @RequestBody UpdatePcCreateRequest request) {
        Pcbase pcbase = pcbaseService.getData(pabaseId);
        updatePcService.setUpdatePc(pcbase, request);

        return "Ok";
    }

    @GetMapping("/all")
        public List<UpdatePcItem> getUpdatePcs() {
        return updatePcService.getUpdatePcs();
    }

    @GetMapping("/detail/update-pc-id/{updatePcId}")
    public UpdatePcResponse getUpdatePc(@PathVariable long updatePcId) {
        return updatePcService.getUpdatePc(updatePcId);
    }

    @PutMapping("/change/update-pc-id/{updatePcId}")
    public String putChange(@PathVariable long updatePcId, @RequestBody UpdatePcChangeRequest request) {
        updatePcService.putChange(updatePcId, request);

        return "OK";
    }

    @DeleteMapping("/update-pc-id/{updatePcId}")
    public String delUpdatePc(@PathVariable long updatePcId) {
        updatePcService.delUpdatePc(updatePcId);

        return "Ok";
    }

}
