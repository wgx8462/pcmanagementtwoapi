package org.gb.pcmanagementtwoapi.model.updatepc;

import lombok.Getter;
import lombok.Setter;


import java.time.LocalDate;

@Getter
@Setter
public class UpdatePcItem {
    private String pcbaseRunStatus;
    private String updateType;
    private LocalDate dateUpCheck;
}
